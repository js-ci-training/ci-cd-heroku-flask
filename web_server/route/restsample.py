#!flask/bin/python
import functools

from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for,make_response
)

bp = Blueprint('restsample', __name__, url_prefix='/rest')


@bp.route('/json', methods=('GET', 'POST'))
def authBasic():
   
    if request.method == 'POST':
        json={"method":"POST","data":{"a1":"value1","a2":"value2"}}
    elif request.method == 'GET':
        json={"method":"GET","data":{"a1":"value1","a2":"value2"}}
    return json

import os

from flask import Flask,render_template
from web_server.route import restsample

# create and configure the app
app = Flask(__name__)
app.register_blueprint(restsample.bp)

# a simple page that says hello
@app.route('/hello')
def hello():
    return 'Hello, World!'

@app.route('/')
def start():
    return render_template('login.html', msg="This is my message to display")


if __name__ == "__main__":
    app.run(threaded=True, port=5000)

# CI-CD-Heroku-flask : Continuous Delivery

##  1. Contexte
Dans ce tuto. nous allons voir comment déployer une serveur Flask sur une instance ```Herok```.

## 2. Création d'un application Flask simple
- Créer un environnement virtuel pour les dépendances python

```
$ python3 -m venv venv
$ source ./venv/bin/activate
```
- A la racine du repo. créer le fichier ```requirements.txt``` comme suit:
```
# Flask Framework
Flask==1.1.1

# Flask Packages
Flask-Login==0.4.0
Flask-Migrate==2.0.2
Flask-Script==2.0.5
Flask-SQLAlchemy==2.4.0
Flask-WTF==0.14.2
Flask-User==1.0.1.5

# Automated tests
pytest==3.0.5
pytest-cov==2.4.0

gunicorn==19.0.0
```


- Installer les dépendances du requirements.txt
```
(venv)$ pip install -r requirements.txt
```

- Créer un répertoire ```web_server```

```
(venv)$ mkdir web_server
```
- Créer un fichier ```app.py``` qui va contenir le point d'entrée du serveur Flask:

```python
import os

from flask import Flask,render_template
from web_server.route import restsample

# create and configure the app
app = Flask(__name__)
app.register_blueprint(restsample.bp)

# a simple page that says hello
@app.route('/hello')
def hello():
    return 'Hello, World!'

@app.route('/')
def start():
    return render_template('login.html', msg="This is my message to display")


if __name__ == "__main__":
    app.run(threaded=True, port=5000)

```

- Créer un répertoire dans  ```web_server``` nommé ```static```. Ce répertoire va contenir tous les fichiers accessibles directement par un Web Browser:

```
(venv)$ mkdir static
```
- Créer un fichier ```test1.html``` dans le répertoire ```static``` comme suit:

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Test Static file</title>    
</head>
    <body>
        <h1>STATIC FILE TEST 1</h1>
    </body>
</html>
```


- Créer un répertoire dans  ```web_server``` nommé ```route```. Ce répertoire va contenir  les fichiers python qui vont permettre de définir des Entry Points sur notre server Web:

```
(venv)$ mkdir route
```
- Créer un fichier vide ```__init__.py``` afin de préciser à python qu'il s'agit d'un package python

```
(venv)$ cd route
(venv)$ touch __init__.py
```
- Dans le répertoire ```route```, créer le fichier ```restsample.py``` comme suit:

```python 
#!flask/bin/python
import functools

from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for,make_response
)

bp = Blueprint('restsample', __name__, url_prefix='/rest')


@bp.route('/json', methods=('GET', 'POST'))
def authBasic():
   
    if request.method == 'POST':
        json={"method":"POST","data":{"a1":"value1","a2":"value2"}}
    elif request.method == 'GET':
        json={"method":"GET","data":{"a1":"value1","a2":"value2"}}
    return json
```
- Explications:
  
  - Ce fichier permet de définir le comportement de l'application lorsque l'url ```/rest/json``` est appelée (ici un object JSON est retourné au WebBrowser)
  ```python
  bp = Blueprint('restsample', __name__, url_prefix='/rest')
  ```
  - On indique ici le nom de la route ainsi que le préfixe pour toutes les URLs définies dans ce fichier. Lorsqu'on définira une route, le prefixe ```/rest``` sera appliqué (e.g ```@bp.route('/json')...``` sera accessible via l'URL ```/rest/json```.
  ````python
  @bp.route('/json', methods=('GET', 'POST'))
    def authBasic():
        if request.method == 'POST':
        json={"method":"POST","data":{"a1":"value1","a2":"value2"}}
        ...
        return json
  ````
   - On définit une nouvelle route qui sera accessible en HTTP Post ou HTTP Get
   - ```request.method``` permet de récupérer la méthode HTTP associée à la requête.
   - ```json={"method":"POST","data":...``` création d'un objet JSON
   - ```return json``` on retourne au Web Browser l'objet JSON.


- Créer un répertoire dans  ```web_server``` nommé ```template```. Ce répertoire va contenir  les fichiers template html de Flask qui vont être complétés par Flask au besoin:

```
(venv)$ mkdir template
```
- Dans le répertoire ```template``` créer le fichier ```login.html``` suivant:

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Register</title>
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css" />

    
</head>
<body>
<h1>{{ msg }}</h1>
<form class="ui form" action="/auth/login" method="POST">
    <div class="field">
      <label>User Name</label>
      <input id="iptusername" type="text" name="username" placeholder="User Name">
    </div>
    <div class="field">
      <label>Password</label>
      <input id="iptpwd" type="password" name="password" placeholder="Passord">
    </div>
    
    <button id="btnsubmit" class="ui button" type="button">Submit</button>
  </form>
</body>
</html

```
- ```{{ msg }}``` est le format qui permet à Flask de savoir quelles parties du fichier .html il devra remplacer/compiler. La valeur de ```msg``` est fixée lors du traitement de la requête de la racine du server Web ```\```. On retrouve dans le fichier ```app.py``` ce traitement:

```python
...
 @app.route('/')
    def start():
        return render_template('auth/login.html', msg="This is my message to display")
...

```

- Vous devriez avoir l'arborescence finale suivante:

```
- web_server
    |-route
        |-__init__.py
        |-restsample.py
    |-static
        |-test1.html
    |-templates
        |-login.html
    |-__init__.py
    |-app.py
```

- Démarrer votre serveur Web:
```
(venv)$ cd web_server
(venv)$ export FLASK_APP=app.py
(venv)$ flask run
```
- Vous devriez avoir le résultat suivant:

```
 * Environment: production
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
 * Debug mode: off
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
```
- Dans un navigateur Web tester que votre server web est accessible à l'URL suivante:

```
http://127.0.0.1:5000/
```
## 3 Configurer le Continuous Delivery
La suite de ce tuto suit les recommandations indiquées ici (https://medium.com/swlh/how-do-i-deploy-my-code-to-heroku-using-gitlab-ci-cd-6a232b6be2e4)

### 3.1 Configurer de Heroku

- Créer un compte chez HEROKU https://dashboard.heroku.com/
- Générer une clé d'API dans ```account settings```> ```Real```

![Heroku Api Key](https://miro.medium.com/max/1400/1*XDHTONpx11tznVEpMi3wRA.png)

- Créer une nouvelle application 
![Heroku create App1](https://miro.medium.com/max/2000/1*w23kah7lvKDazyqzoQs2PA.png)
![Heroku create App2](https://miro.medium.com/max/2000/1*PVniAX6WPTDtaaMxE-LhJA.png)

### 3.2 Configurer de Gilab
- Dans votre projet Gilab ouvir le menu ```settings > CI/CD```
- Vérifier que les ```runner``` Gitlab sont activés

![Gitlab Runner](https://miro.medium.com/max/2000/1*C3QSrYdIhHT3BcEar8sefw.png)

- Aller dans la section ```Variables``` et ajouter les 3 variables suivantes:
  - HEROKU_API_KEY : clé de votre Api Heroku
  - HEROKU_APP_PRODUCTION : nom de votre app en stage
  - HEROKU_APP_STAGING : nom de votre app en prod

![Gilab variables](https://miro.medium.com/max/2000/1*T3_zNAc7rjTEAFg0IQXppA.png)

- A la racine de votre projet Gitlab, créer le fichier ```.gitlab-ci.yml``` comme suit:

```yaml

stages:
    - production

production:
    type: deploy
    stage: production
    image: python:3.7
    script:
        - apt-get update -qy
        - apt-get install -y ruby-dev
        - gem install dpl
        - dpl --provider=heroku --app=$HEROKU_APP_PRODUCTION --api-key=$HEROKU_API_KEY
    only:
        - master

```

- Création d'un fichier permettant d'informer les éléments à lancer par Heroku: créer le fichier ```Procfile``` à la racine comme suit:

```
web: gunicorn web_server.app:app
```

- Faite une modification et pousser votre modification sur master. 
- Dans Git lab regarder l'évolution de votre jos en cours ```CI\CD``` > ```jobs``` cliquer sur le job ```running``` pour voir le détail. Si tout se passe bien votre application a été déployée sur Heroku.

## 4 Tester votre application

Dans un Web Browser tester les URLs suivantes:

- ``` https://<your app name>.herokuapp.com/ ``` : l'appel à la racine de votre application doit donner le résultat suivant :

<img alt="img result racine" src="./images/result1.jpg" width="300">



- ``` https://<your app name>.herokuapp.com/hello ``` : l'appel doit donner le résultat suivant :

<img alt="img result hello" src="./images/result2.jpg" width="300">


- ``` https://<your app name>.herokuapp.com/rest/json ``` : l'appel doit donner le résultat suivant :

<img alt="img result json" src="./images/result3.jpg" width="300">
